{{ config(materialized='table',database='DBT_DB', 
    alias='CDT_USG_BY_WH_OP_TOOL')}}

select name as "Account"
,warehouse_name as "WH Name"
,op_name as "Op Name"
,month(time) as "Month"
,warehouse_size as "WH Size"
,warehouse_mult as "WH Mult"
,sum(total_duration_secs/60) as "Total Duration Mins"
,sum(credits_consumed) as "TOTAL CREDITS"
,sum(rows_produced) as "ROWS PRODUCED"
,sum(rows_inserted) as "ROWS INSERTED"
,sum(rows_updated) as "ROWS UPDATED"
,sum(rows_deleted) as "ROWS DELETED"
,sum(partitions_scanned) as "PARTITIONS SCANNED"
,sum(partitions_total) as "PARTITIONS TOTAL"
,sum(bytes_spilled_to_local_storage/1024/1024/1024) as "GB Spilled to Local Storage"
,sum(bytes_spilled_to_remote_storage/1024/1024/1024) as "GB Spilled to Remote Storage"
,count(*) as "Query Count"
from {{source('source_1','qh_transformed')}}
where 1=1
//and time > $START_TIME_MIN --'2021-11-01'
//and time < $START_TIME_MAX --'2021-12-01'
and time > timestampadd(day, -1, current_timestamp)
and time < current_timestamp
and sql_text like '%PM\\_%' escape '\\'
and cluster_number is not null -- filter out cs only stuff (could also test against warehouse_mult)
group by 1,2,3,4,5,6
order by 1,2,3,4,5,6

