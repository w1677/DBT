{{ config(materialized='incremental',unique_key='date', transient=false,alias='test', incremental_strategy="delete+insert")}}

select * from "TEST"."TEST_SCHEMAS"."PRODUCT"

{% if target.name == 'dev' %}

where date<= current_date

{% endif %}

{%if is_incremental() %} 
and date >= (select max(date) from {{ this }})
{% endif %}