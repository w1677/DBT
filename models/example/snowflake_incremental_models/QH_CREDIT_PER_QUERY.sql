
{{ config(materialized='view', alias='qh_credits_per_query')}}

select qid,QUERY_TEXT
,sum(segment_credits) as credits_consumed
from {{ref('QH_CREDIT')}}
group by qid,QUERY_TEXT
