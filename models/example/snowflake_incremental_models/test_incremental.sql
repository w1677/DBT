{{ config(materialized='incremental',unique_key='o_orderdate', transient=false)}}

select * from "SNOWFLAKE_SAMPLE_DATA"."TPCH_SF1"."ORDERS" 
where o_orderdate<= current_date

{%if is_incremental() %} 
and o_orderdate > (select max(o_orderdate {{this}})
{% endif %}