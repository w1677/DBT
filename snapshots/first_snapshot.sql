{% snapshot first_snapshot %}
   
     {{ config (
         target_database='dbt_db',
         target_schema='snapshot',
         unique_key='id',

       strategy='timestamp',
       updated_at='updated_at'
     )
     }}
     
     --tips:use source in snapshot

     select * from {{ref('my_first_dbt_model')}}

    {% endsnapshot %}
